const express = require('express');
const router = express.Router();
const listController = require('../controllres/listController');
const {auth, optionalAuth} = require('../middleware/auth');

router.get('/', optionalAuth, listController.getLists.bind(listController));
router.post('/', auth, listController.createList.bind(listController));
router.put('/:listId', auth, listController.updateList.bind(listController));
router.delete('/:listId', auth, listController.removeList.bind(listController));

router.get('/:listId/', optionalAuth, listController.getList.bind(listController));
router.post('/:listId/', auth, listController.addItem.bind(listController));
router.put('/:listId/:itemId/', auth, listController.updateItem.bind(listController));
router.delete('/:listId/:itemId', auth, listController.removeItem.bind(listController));

router.get('/:listId/:itemId/file', auth, listController.getItemFile.bind(listController));

module.exports = router;
