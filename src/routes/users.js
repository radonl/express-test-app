const express = require('express');
const router = express.Router();
const usersController = require('../controllres/usersController');
const {auth} = require('../middleware/auth');

router.post('/', usersController.createUser);
router.post('/login', usersController.login);
router.post('/logout', auth, usersController.logout);
router.post('/logoutall', auth, usersController.logoutAll);

module.exports = router;
