const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ListModelSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    isPublic: {
        type: String,
        required: true
    },
    owner : { type: Schema.Types.ObjectId, ref: 'User' }
});


module.exports = mongoose.model('List', ListModelSchema);
