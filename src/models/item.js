const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemModelSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    listId : { type: Schema.Types.ObjectId, ref: 'List' },

    file: {
        savedFileName: {
            type: String,
            trim: true
        },
        originalFileName: {
            type: String,
            trim: true
        }
    }
});


module.exports = mongoose.model('Item', ItemModelSchema);
