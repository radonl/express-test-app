'use strict';

const jwt = require('jsonwebtoken');
const User = require('../models/user');
const formatServicesResponse = require('../helpers/formatServicesResponse');
const formatResponse = require('../helpers/formatResponse');

const notAuthorized = res => formatResponse(formatServicesResponse.notAuthorized(), res);

const userRemoved = res => formatResponse(formatServicesResponse.notAuthorized('User has been deleted!'), res);

const auth = async (req, res, next) => await authHandle(req, res, next, true);

const optionalAuth = async (req, res, next) => await authHandle(req, res, next, false);

const authHandle = async (req, res, next, isRequired) => {
  try {
    const token = req.header('Authorization');
    if (token) {
      const data = jwt.verify(token, process.env.JWT_KEY);
      const user = await User.findOne({_id: data._id, 'tokens.token': token});
      if (!user && isRequired) {
        userRemoved(res);
      }
      req.user = user;
      req.token = token;
      next();
    } else if (isRequired) {
      notAuthorized(res);
    } else {
      next();
    }
  } catch (e) {
    if (isRequired) {
      notAuthorized(res);
    } else {
      next();
    }
  }
};

module.exports = {auth, optionalAuth};
