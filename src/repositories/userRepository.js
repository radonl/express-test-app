const User = require('../models/user');
const bcrypt = require('bcryptjs');

class UserRepository {
    async findByCredentials (email, password) {
        try {
            const user = await User.findOne({email});
            if (!user) {
                return false;
            }
            const isPasswordMatch = await bcrypt.compare(password, user.password);
            if (!isPasswordMatch) {
                return false;
            }
            return user;
        } catch (e) {
            throw e;
        }
    }

    async create(body) {
        try {
            const user = new User(body);
            await user.save();
            return user;
        } catch (e) {
            throw e;
        }
    }

    async logout(user, token) {
        try {
            user.tokens = user.tokens.filter((token) => {
                return token.token !== token
            });
            await user.save();
        } catch (e) {
            throw e;
        }
    }

    async logoutAll(user) {
        try {
            user.tokens.splice(0, user.tokens.length);
            await user.save();
        } catch (e) {
            throw e;
        }
    }
}

module.exports = new UserRepository();
