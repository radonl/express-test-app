const List = require('../models/list');
const Item = require('../models/item');

class ListRepository {
    async getLists(owner) {
        try {
            return await List.find({
                $or: [{
                    isPublic: true
                }, {owner}]
            });
        } catch (e) {
            throw e;
        }
    }

    async createList(body) {
        try {
            const list = new List(body);
            await list.save();
            return list;
        } catch (e) {
            throw e;
        }
    }

    async updateList(list, body) {
        try {
            Object.keys(body).forEach(key => list[key] = body[key]);
            await list.save();
            return list;
        } catch (e) {
            throw e;
        }
    }

    async getList(id) {
        try {
            return await List.findOne({_id: id})
        } catch (e) {
            throw e;
        }
    }

    async removeList(list) {
        try {
            await list.remove();
            return list.id;
        } catch (e) {
            throw e;
        }
    }

    async getListItems(listId) {
        try {
            return Item.find({
                listId
            })
        } catch (e) {
            throw e;
        }
    }

    async createItem(body) {
        const item = new Item(body);
        await item.save();
        return item;
    }

    async getItem(id) {
        try {
            return await Item.findOne({_id: id});
        } catch (e) {
            throw e;
        }
    }

    async updateItem(item, body) {
        try {
            Object.keys(body).forEach(key => item[key] = body[key]);
            await item.save();
            return item;
        } catch (e) {
            throw e;
        }
    }

    async removeItem(item) {
        try {
            await item.remove();
            return item;
        } catch (e) {
            throw e;
        }
    }
}

module.exports = new ListRepository();
