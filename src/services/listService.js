'use strict';

const fs = require('fs');
const crypto = require('crypto');

const listRepository = require('../repositories/listRepository');
const formatServicesResponse = require('../helpers/formatServicesResponse');

class ListService {
  createFile(file) {
    if (file && file.content && file.extension) {
      const fileName = `${crypto.createHash('md5').update(file.content).digest('hex')}.${file.extension}`;
      const stream = fs.createWriteStream(`storage/${fileName}`);
      const buffer = Buffer.from(file.content, 'base64');
      stream.write(buffer);
      stream.end();
      return fileName;
    }
    return false;
  }

  async getLists(owner) {
    try {
      const result = await listRepository.getLists(owner);
      return formatServicesResponse.success(result);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async createList(body) {
    try {
      const result = await listRepository.createList(body);
      return formatServicesResponse.success(result, true);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async updateList(listId, body, user) {
    try {
      const list = await listRepository.getList(listId);
      if (!list) {
        return formatServicesResponse.notFound('List');
      }
      if (list.owner.toString() === user.id) {
        const result = await listRepository.updateList(list, body);
        return formatServicesResponse.success(result, true);
      }
      return formatServicesResponse.notHavePermission('edit', 'list');
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async removeList(listId, user) {
    try {
      const list = await listRepository.getList(listId);
      if (!list) {
        return formatServicesResponse.notFound('List');
      }
      if (list.owner.toString() === user.id) {
        const result = await listRepository.removeList(list);
        return formatServicesResponse.success(result, true);
      }
      return formatServicesResponse.notHavePermission('remove', 'list');
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async getListItems(listId, user) {
    try {
      const list = await listRepository.getList(listId);
      if (!list) {
        return formatServicesResponse.notFound('List');
      }
      const result = await listRepository.getListItems(listId);
      return formatServicesResponse.success(result);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async createItem(listId, user, body) {
    try {
      const list = await listRepository.getList(listId);
      if (!list) {
        return formatServicesResponse.notFound('List');
      }
      if (list.owner.toString() === user.id) {
        const savedFileName = this.createFile(body.file);
        const result = await listRepository.createItem({
          ...body,
          listId: list._id,
          ...savedFileName ? {
            file: {
              savedFileName,
              originalFileName: body.file.name
            }
          } : {}
        });
        return formatServicesResponse.success(result, true);
      }
      return formatServicesResponse.notHavePermission('add item to', 'list');
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async updateItem(itemId, listId, user, body) {
    try {
      const [item, list] = await Promise.all([
        listRepository.getItem(itemId),
        listRepository.getList(listId)
      ]);
      if (!list) {
        return formatServicesResponse.notFound('List');
      }
      if (!item) {
        return formatServicesResponse.notFound('Item');
      }
      if (list.owner.toString() !== user.id) {
        return formatServicesResponse.notHavePermission('edit', 'list');
      }
      if (list.id !== item.listId.toString()) {
        return formatServicesResponse.notFound('Item');
      }
      const savedFileName = this.createFile(body.file);
      const result = await listRepository.updateItem({
        ...body,
        listId: list._id,
        ...savedFileName ? {
          file: {
            savedFileName,
            originalFileName: body.file.name
          }
        } : {}
      });
      return formatServicesResponse.success(result, true);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async removeItem(itemId, listId, user) {
    try {
      const [item, list] = await Promise.all([
        listRepository.getItem(itemId),
        listRepository.getList(listId)
      ]);
      if (!list) {
        return formatServicesResponse.notFound('List');
      }
      if (!item) {
        return formatServicesResponse.notFound('Item');
      }
      if (list.owner.toString() !== user.id) {
        return formatServicesResponse.notHavePermission('remove item from', 'list');
      }
      if (list.id !== item.listId.toString()) {
        return formatServicesResponse.notFound('Item');
      }
      return formatServicesResponse.success(await listRepository.removeItem(item), true);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async getFile(itemId, listId, user) {
    try {
      const [item, list] = await Promise.all([
        listRepository.getItem(itemId),
        listRepository.getList(listId)
      ]);
      return (list && item && list.owner.toString() === user.id) ? item.file : false;
    } catch (e) {
      return false;
    }
  }
}

module.exports = new ListService();
