const jwt = require('jsonwebtoken');
const userRepository = require('../repositories/userRepository');

const formatServicesResponse = require('../helpers/formatServicesResponse');

class UserService {
    async generateToken(user) {
        try {
            const token = jwt.sign({_id: user._id}, process.env.JWT_KEY);
            user.tokens = user.tokens.concat({token});
            await user.save();
            return token;
        } catch (e) {
            throw e;
        }
    }

    async login({email, password}) {
        try {
            const user = await userRepository.findByCredentials(email, password);
            if(!user) {
                return formatServicesResponse.notAuthorized('Authorization failed!');
            }
            const token = await this.generateToken(user);
            return formatServicesResponse.success({user, token});
        } catch (e) {
            return formatServicesResponse.exception(e);
        }
    }

    async create(body) {
        try {
            const user = await userRepository.create(body);
            const token = await this.generateToken(user);
            return formatServicesResponse.successCreated({user, token});
        } catch (e) {
            return formatServicesResponse.exception(e);
        }
    }

    async logout(user, token) {
        try {
            await userRepository.logout(user, token);
            return formatServicesResponse.success();
        } catch (e) {
            return formatServicesResponse.exception(e);
        }
    }

    async logoutAll(user) {
        try {
            await userRepository.logoutAll(user);
            return formatServicesResponse.success();
        } catch (e) {
            return formatServicesResponse.exception(e);
        }
    }
}

module.exports = new UserService();
