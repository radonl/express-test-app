let connection = null;

class SocketsService {
  constructor() {
    this.sockets = new Map();
  }
  connect(server) {
    const io = require('socket.io')(server);
    io.on('connection', (socket) => {
      this.sockets.set(socket.id, socket);
      socket.on('disconnect',  () => {
        console.log(socket.id,"Disconnect");
        this.sockets.delete(socket.id);
      });
      console.log(`New socket connection: ${socket.id}`);
    });
  }

  sendEvent(event, data) {
    [...this.sockets.keys()].forEach(key => this.sockets.get(key).emit(event, data));
  }

  registerEvent(event, handler) {
    [...this.sockets.keys()].forEach(key => this.sockets.get(key).emit(event, handler));
  }

  static init(server,sessionMiddleware) {
    if(!connection) {
      connection = new SocketsService();
      connection.connect(server,sessionMiddleware);
    }
  }

  static getConnection() {
    if(!connection) {
      throw new Error("no active connection");
    }
    return connection;
  }
}

module.exports = {
  connect: SocketsService.init,
  connection: SocketsService.getConnection
};
