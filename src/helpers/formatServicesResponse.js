exports.notHavePermission = (action, entityName) => ({
    status: 403,
    response: {
      status: false,
      error: `You do not have permission to ${action} this ${entityName}!`
    }
});

exports.notFound = (entityName) => ({
    status: 404,
    response: {
      status: false,
      error: `${entityName} not found!`
    }
});

exports.success = (data, isForSocket = false) => ({
    status: 200,
    response: {
      status: true,
      ...data && !isForSocket ? {data} : {}
    },
    ...isForSocket ? {socketData: {data}} : {}
});

exports.successCreated = (data) => ({
  status: 201,
  response: {
    status: true,
    ...data ? {data} : {}
  }
});

exports.notAuthorized = (message) => ({
  status: 401,
  response: {
    status: false,
    ...message ? {message} : {message: 'User not authorized!'}
  }
});

exports.exception = (e) => e.name === 'ValidationError' ? {
  status: 422,
  response: {
    status: false,
    ...e
  }
} : {
  status: 500,
  response: {
    status: false,
    error: 'Internal server error'
  }
};

