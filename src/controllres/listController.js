'use strict';

const fs = require('fs');

const listService = require('../services/listService');
const formatResponse = require('../helpers/formatResponse');
const socketService = require('../services/socketsService');

class ListController {
  async getLists(req, res) {
    try {
      const ownerId = (req.user && req.user._id) ? req.user._id : null;
      const result = await listService.getLists(ownerId);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  };

  async createList(req, res) {
    try {
      const result = await listService.createList({
        ...req.body,
        owner: req.user._id
      });
      socketService.connection().sendEvent(`${req.user.id}.ListCreated`, result.socketData);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  };

  async updateList(req, res) {
    try {
      const result = await listService.updateList(req.params.listId, req.body, req.user);
      socketService.connection().sendEvent(`${req.user.id}.ListUpdated`, result.socketData);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  };

  async removeList(req, res) {
    try {
      const result = await listService.removeList(req.params.listId, req.user);
      socketService.connection().sendEvent(`${req.user.id}.ListRemoved`, result.socketData);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  };

  async getList(req, res) {
    try {
      const result = await listService.getListItems(req.params.listId, req.user);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  };

  async addItem(req, res) {
    try {
      const result = await listService.createItem(req.params.listId, req.user, req.body);
      socketService.connection().sendEvent(`${req.user.id}.ItemAdded`, result.socketData);
      formatResponse(result, res);
    } catch (e) {
      console.log(e);
      res.status(500).send();
    }
  };

  async updateItem(req, res) {
    try {
      const result = await listService.updateItem(req.params.itemId, req.params.listId, req.user, req.body);
      socketService.connection().sendEvent(`${req.user.id}.ItemUpdated`, result.socketData);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  };

  async removeItem(req, res) {
    try {
      const result = await listService.removeItem(req.params.itemId, req.params.listId, req.user);
      socketService.connection().sendEvent(`${req.user.id}.ItemRemoved`, result.socketData);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  };

  async getItemFile(req, res) {
    const {savedFileName, originalFileName} = await listService.getFile(req.params.itemId, req.params.listId, req.user);
    const filePath = `storage/${savedFileName}`;
    if (!savedFileName && !originalFileName && !fs.existsSync(filePath)) {
      return res.status(403).send();
    }
    const stat = fs.statSync(filePath);
    res.writeHead(200, {
      'Content-Type': 'application/octet-stream',
      'Content-Length': stat.size
    });

    const readStream = fs.createReadStream(filePath);
    readStream.pipe(res);
  }
}

module.exports = new ListController();
