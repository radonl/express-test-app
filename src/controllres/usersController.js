'use strict';

const userService = require('../services/userService');
const formatResponse = require('../helpers/formatResponse');

class UsersController {
  async createUser(req, res) {
    try {
      const result = await userService.create(req.body);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  }

  async login(req, res) {
    try {
      const result = await userService.login(req.body);
      formatResponse(result, res);
    } catch (e) {
      res.status(500).send();
    }
  }

  async logout(req, res) {
    try {
      const result = await userService.logout(req.user, req.token);
      formatResponse(result, res);
    } catch (error) {
      res.status(500).send();
    }
  }

  async logoutAll(req, res) {
    try {
      const result = await userService.logoutAll(req.user);
      formatResponse(result, res);
    } catch (error) {
      res.status(500).send();
    }
  }
}

module.exports = new UsersController();
